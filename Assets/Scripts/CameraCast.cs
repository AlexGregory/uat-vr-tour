﻿using UnityEngine;
using System.Collections;

//Virtual Tours, Last Edited: Kenneth Vorseth, 2/13/2017
[RequireComponent(typeof(Camera))]
public class CameraCast : MonoBehaviour
{
    [Header("Raycast Settings / References")]
    public float raycastFreq; //.............Time Measured in Seconds, Between Raycast Fires
    public float raycastDist; //.............Distance of Raycast, Measured in Meters (OnLookAtEnter, OnLookAtStay, OnLookAtExit)
    public GameObject laserPrefab; //........Laser Pointer Prefab
    private GameObject laserPointer;//.......Laser Pointer Object Instance
    private float holdTimer = 0.0f;
    private GameObject lastHitObject = null;
    private GameObject currentHitObject = null;
    private enum HitState {None, Enter, Stay, Exit}
    private HitState hState = HitState.None;

    void Start()
    {
        //Create a Laser Pointer Instance
        laserPointer = GameObject.Instantiate<GameObject>(laserPrefab);
        laserPointer.SetActive(false);
    }

    void Update()
    {
        //Fire Raycast at Sequence
        holdTimer += Time.deltaTime;
        if (holdTimer >= raycastFreq)
        {
            //Fire Raycast
            RaycastFire();
            //Reset Timer
            holdTimer = 0.0f;
        }
        //Update LookAt Handling
        UpdateLookAtMessages();
    }

    void UpdateLookAtMessages()
    {
        //Determines Current LookAt State Determined By Raycast Output
        switch (hState)
        {
            case HitState.None:
                if (lastHitObject == null && currentHitObject != null)
                    hState = HitState.Enter;
                break;
            case HitState.Enter:
                lastHitObject.SendMessage("OnLookAtEnter");
                hState = HitState.Stay;
                break;
		case HitState.Stay:
				if (lastHitObject != null) {
					lastHitObject.SendMessage ("OnLookAtStay");
				}
				if (lastHitObject != null && currentHitObject != lastHitObject)
                { lastHitObject.SendMessage("OnLookAtExit"); hState = HitState.Exit; }
                break;
            case HitState.Exit:
                hState = HitState.None;
                break;
        }
        //Set Last Hit Object for Transition Reference Above
        lastHitObject = currentHitObject;
    }

    void RaycastFire()
    {
        //Initiate Ray
        Transform tf = gameObject.GetComponent<Transform>();
        Ray ray = new Ray(tf.position, tf.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, raycastDist))
        {
            //Grab Tag from Name
            string hitTag = ReturnTag(hit.collider.name);
            if (hitTag == "FuseBtn") //Fuse Btn Raycast Hit
            {
                //Set Hit Object
                currentHitObject = hit.collider.gameObject;
                //Display Laser Pointer
                laserPointer.SetActive(true);
                laserPointer.transform.position = hit.point;
                laserPointer.transform.LookAt(Camera.main.transform.position);
            }
            else
            {
                //Nullify Hit Object
                currentHitObject = null;
                //HIde Laser Pointer if No Hit
                laserPointer.SetActive(false);
            }
        }
        else
        {
            //Nullify Hit Object
            currentHitObject = null;
            //Hide Laser Pointer if No Hit
            laserPointer.SetActive(false);
        }
    }

    //Returns Tag of Object Hit From Raycast
    string ReturnTag(string originalName)
    {
        //Delimetes Tag From Name
        char[] delimeter = { '~' };
        string[] nameDelimeted = originalName.Split(delimeter);
        if (nameDelimeted.Length >= 2)
        {
            return nameDelimeted[1];
        }
        else
            return null;
    }
}
