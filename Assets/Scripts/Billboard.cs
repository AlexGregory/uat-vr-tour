﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		// Look at the camera every frame draw
		tf.LookAt(Camera.main.transform.position);
		tf.Rotate(new Vector3(0,180,0));
	}
}
