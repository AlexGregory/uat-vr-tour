﻿using UnityEngine;
using System.Collections;

//Last Edited: Kenneth Vorseth, 2/15/2017
public class TagComponent : MonoBehaviour
{
    [Header("Tag Definition")]
    public string tagDef = "None";

    void Start()
    {
        //Concatenate the Tag with Delimeter onto Name
        name += "~" + tagDef;
    }
}
