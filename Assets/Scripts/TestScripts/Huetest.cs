﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Huetest : MonoBehaviour {

	public FuseButton buttonToTest;
	public KeyCode testKey = KeyCode.Return;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (testKey)) {
			buttonToTest.OnLookAtEnter ();
		}
		if (Input.GetKey (testKey)) {
			buttonToTest.OnLookAtStay ();
		}
		if (Input.GetKeyUp (testKey)) {
			buttonToTest.OnLookAtExit ();
		}
	}
}
