﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FuseButton))]
public class InfoButton : MonoBehaviour
{
    [Header("References/Links: InfoButton")]
    public Animation anim;

    void OnFuseButtonActivate()
    {
        anim.Play("Open");   
    }
    void OnLookAtExit()
    {
        anim.Play("Close");
    }
}
