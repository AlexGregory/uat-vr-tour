﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour 
{
	[Header("Panel Links")]
	public GameObject creditsPanel;

	public void OnStartTour()
	{
		SceneManager.LoadScene (1);
	}
	public void OnCredits()
	{
		if (creditsPanel.activeSelf) {
			creditsPanel.SetActive (false);
		} else 
		{
			creditsPanel.SetActive (true);
		}
	}
	public void OnContact()
	{
		Application.OpenURL ("http://www.uat.edu/request-info?from=vrtour");
	}
	public void OnQuit()
	{
		Application.Quit ();
	}
}
