﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour {
	public Renderer render;
	public Color colors;
	public bool fadeIn;
	public bool fadeOut;
	public float fadeSpeed;

	// Use this for initialization
	void Start () {
		render = gameObject.GetComponent<Renderer> ();
		colors = render.material.color; 

		StartCoroutine ("FadeIn");
	}
	
	// Update is called once per frame
	void Update () {
	}

	public IEnumerator FadeIn() {
	
		while (colors.a > 0) {
			colors.a -= fadeSpeed * Time.deltaTime;
			render.material.color = colors;
			yield return null;
		}


		yield return null;
	}

	public IEnumerator FadeOut() {

		while (colors.a < 1) {
			colors.a += fadeSpeed * Time.deltaTime;
			render.material.color = colors;
			yield return null;
		}

		yield return null;
	}

}
