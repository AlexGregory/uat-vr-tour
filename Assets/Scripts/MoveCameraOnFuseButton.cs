﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraOnFuseButton : MonoBehaviour {

	[Tooltip("If null, then move Main Camera")] public Camera cameraToMove;
	[Tooltip("Target object to move to")] public Transform targetTransform;
	[Tooltip("If No Target Object Set, go to this position")] public Vector3 positionIfNoTarget;
    //[Tooltip("X Rotation of the next sphere")] public float nextSphereXrotation;
    //[Tooltip("Y Rotation of the next sphere")] public float nextSphereYrotation;
    //[Tooltip("Z Rotation of the next sphere")] public float nextSphereZrotation;

    // Fader


    public void OnFuseButtonActivate () {
		if (cameraToMove == null) {
			cameraToMove = Camera.main;
		}

		if (targetTransform != null) {
			positionIfNoTarget = targetTransform.position;
            //targetTransform.eulerAngles = new Vector3(nextSphereXrotation,targetTransform.eulerAngles.y,nextSphereZrotation); 
		}

		StartCoroutine ("MoveCamera");
	}

	public IEnumerator MoveCamera(){

		Fade fader = cameraToMove.GetComponentInChildren<Fade>();

		// wait for fade out
		if (fader != null) {
			yield return fader.StartCoroutine ("FadeOut");
		}

		cameraToMove.transform.position = positionIfNoTarget;

		// wait for fade in
		if (fader != null) {
			yield return fader.StartCoroutine ("FadeIn");
		}

		yield return null;
	}

}
