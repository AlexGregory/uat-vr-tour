﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FuseButton : MonoBehaviour {

	public Image centerImage;
	public Image spiralImage;
	public Text countdownTextbox;
	public float totalTime;

	public Text roomText;
	public string roomTitle;

	// Privates
	private float currentTime;
	private bool hasFired = false;

	// Components
	private Transform tf;


	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		currentTime = 0.0f;
		hasFired = false;

		UpdateDisplay ();

		// TODO: Start object as faded out
		countdownTextbox.text = "";
		roomText.text = roomTitle;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// On "gaze"
	public void OnLookAtStay() {
		if (hasFired)
			return;

		// Advance the timer
		currentTime += Time.deltaTime;

		// Update display
		UpdateDisplay();

		if (currentTime > totalTime) {
			gameObject.SendMessage ("OnFuseButtonActivate", SendMessageOptions.DontRequireReceiver);
			hasFired = true;
		}
	}

	public void OnLookAtEnter() {
		// Set the timer to 0
		currentTime = 0;

		// Update display
		UpdateDisplay ();

		// Set that we haven't fired
		hasFired = false;

		// TODO: Fade In Objects
	}

	public void OnLookAtExit() {
		// Set the timer to 0
		currentTime = 0;

		// Update display
		UpdateDisplay ();

		// Set that we haven't fired
		hasFired = false;

		// TODO: Fade Out Objects
		countdownTextbox.text = "";
	}

	public void UpdateDisplay() {
		// Draw the circle
		spiralImage.fillAmount = currentTime / totalTime;

		// Set the text
		float countdownTime = Mathf.Ceil(totalTime - currentTime);
		if (countdownTime != 0) {
			countdownTextbox.text = "" + Mathf.Ceil (totalTime - currentTime);
		} else {
			countdownTextbox.text = "";
		}

	}

}
